"use strict";
(self["webpackChunkprod_study_project"] = self["webpackChunkprod_study_project"] || []).push([["src_pages_MainPage_ui_MainPage_tsx"],{

/***/ "./src/pages/MainPage/ui/MainPage.tsx":
/*!********************************************!*\
  !*** ./src/pages/MainPage/ui/MainPage.tsx ***!
  \********************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_i18next__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/useTranslation.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");



var MainPage = function MainPage() {
  var t = (0,react_i18next__WEBPACK_IMPORTED_MODULE_1__.useTranslation)().t;
  return (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
    children: t('Главная страница')
  });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MainPage);

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3JjX3BhZ2VzX01haW5QYWdlX3VpX01haW5QYWdlX3RzeC42MTQzMmMwNzQwYmU4ZDU2ODdiNi5qcyIsIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQWdEO0FBQ0Q7QUFDL0MsSUFBSUcsUUFBUSxHQUFHLFNBQVhBLFFBQVEsR0FBZTtFQUN2QixJQUFJQyxDQUFDLEdBQUdGLDZEQUFjLEVBQUUsQ0FBQ0UsQ0FBQztFQUMxQixPQUFPSCxzREFBSSxDQUFDLEtBQUssRUFBRTtJQUFFSSxRQUFRLEVBQUVELENBQUMsQ0FBQyxrQkFBa0I7RUFBRSxDQUFDLENBQUM7QUFDM0QsQ0FBQztBQUNELGlFQUFlRCxRQUFRIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vcHJvZC1zdHVkeS1wcm9qZWN0Ly4vc3JjL3BhZ2VzL01haW5QYWdlL3VpL01haW5QYWdlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbnZhciBNYWluUGFnZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdCA9IHVzZVRyYW5zbGF0aW9uKCkudDtcbiAgICByZXR1cm4gX2pzeChcImRpdlwiLCB7IGNoaWxkcmVuOiB0KCfQk9C70LDQstC90LDRjyDRgdGC0YDQsNC90LjRhtCwJykgfSk7XG59O1xuZXhwb3J0IGRlZmF1bHQgTWFpblBhZ2U7XG4iXSwibmFtZXMiOlsianN4IiwiX2pzeCIsInVzZVRyYW5zbGF0aW9uIiwiTWFpblBhZ2UiLCJ0IiwiY2hpbGRyZW4iXSwic291cmNlUm9vdCI6IiJ9