"use strict";
(self["webpackChunkprod_study_project"] = self["webpackChunkprod_study_project"] || []).push([["src_pages_AboutPage_ui_AboutPage_tsx"],{

/***/ "./src/pages/AboutPage/ui/AboutPage.tsx":
/*!**********************************************!*\
  !*** ./src/pages/AboutPage/ui/AboutPage.tsx ***!
  \**********************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_i18next__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/useTranslation.js");
/* provided dependency */ var __react_refresh_utils__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/lib/runtime/RefreshUtils.js");
/* provided dependency */ var __react_refresh_error_overlay__ = __webpack_require__(/*! ./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js */ "./node_modules/@pmmmwh/react-refresh-webpack-plugin/overlay/index.js");
__webpack_require__.$Refresh$.runtime = __webpack_require__(/*! ./node_modules/react-refresh/runtime.js */ "./node_modules/react-refresh/runtime.js");



var AboutPage = function AboutPage() {
  var t = (0,react_i18next__WEBPACK_IMPORTED_MODULE_1__.useTranslation)('about').t;
  return (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
    children: t('О сайте')
  });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AboutPage);

const $ReactRefreshModuleId$ = __webpack_require__.$Refresh$.moduleId;
const $ReactRefreshCurrentExports$ = __react_refresh_utils__.getModuleExports(
	$ReactRefreshModuleId$
);

function $ReactRefreshModuleRuntime$(exports) {
	if (true) {
		let errorOverlay;
		if (typeof __react_refresh_error_overlay__ !== 'undefined') {
			errorOverlay = __react_refresh_error_overlay__;
		}
		let testMode;
		if (typeof __react_refresh_test__ !== 'undefined') {
			testMode = __react_refresh_test__;
		}
		return __react_refresh_utils__.executeRuntime(
			exports,
			$ReactRefreshModuleId$,
			module.hot,
			errorOverlay,
			testMode
		);
	}
}

if (typeof Promise !== 'undefined' && $ReactRefreshCurrentExports$ instanceof Promise) {
	$ReactRefreshCurrentExports$.then($ReactRefreshModuleRuntime$);
} else {
	$ReactRefreshModuleRuntime$($ReactRefreshCurrentExports$);
}

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3JjX3BhZ2VzX0Fib3V0UGFnZV91aV9BYm91dFBhZ2VfdHN4LmY3NzBjZDU3NGJiZWZmMTBiMzBmLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBZ0Q7QUFDRDtBQUMvQyxJQUFJRyxTQUFTLEdBQUcsU0FBWkEsU0FBUyxHQUFlO0VBQ3hCLElBQUlDLENBQUMsR0FBR0YsNkRBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQ0UsQ0FBQztFQUNqQyxPQUFPSCxzREFBSSxDQUFDLEtBQUssRUFBRTtJQUFFSSxRQUFRLEVBQUVELENBQUMsQ0FBQyxTQUFTO0VBQUUsQ0FBQyxDQUFDO0FBQ2xELENBQUM7QUFDRCxpRUFBZUQsU0FBUyIsInNvdXJjZXMiOlsid2VicGFjazovL3Byb2Qtc3R1ZHktcHJvamVjdC8uL3NyYy9wYWdlcy9BYm91dFBhZ2UvdWkvQWJvdXRQYWdlLnRzeCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IHsgdXNlVHJhbnNsYXRpb24gfSBmcm9tICdyZWFjdC1pMThuZXh0JztcbnZhciBBYm91dFBhZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHQgPSB1c2VUcmFuc2xhdGlvbignYWJvdXQnKS50O1xuICAgIHJldHVybiBfanN4KFwiZGl2XCIsIHsgY2hpbGRyZW46IHQoJ9CeINGB0LDQudGC0LUnKSB9KTtcbn07XG5leHBvcnQgZGVmYXVsdCBBYm91dFBhZ2U7XG4iXSwibmFtZXMiOlsianN4IiwiX2pzeCIsInVzZVRyYW5zbGF0aW9uIiwiQWJvdXRQYWdlIiwidCIsImNoaWxkcmVuIl0sInNvdXJjZVJvb3QiOiIifQ==