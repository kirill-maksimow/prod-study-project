import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { classNames } from 'shared/lib/classNames/classNames';
import { AppLink, AppLinkTheme } from 'shared/ui/AppLink/AppLink';
import { ThemeSwitcher } from 'shared/ui/ThemeSwitcher';
import cls from './NavBar.module.scss';

interface NavBarProps {
  className?: string;
}

export const NavBar = ({ className }: NavBarProps) => {
  const { t } = useTranslation();
  return (
      <div className={classNames(cls.navbar, {}, [className])}>
          <div className={classNames(cls.links, {}, [])}>
              <AppLink
                  theme={AppLinkTheme.SECONDARY}
                  className={classNames(cls.mainLink, {}, [])}
                  to={'/'}>
                  {t('Главная страница')}
              </AppLink>
              <AppLink theme={AppLinkTheme.SECONDARY} to={'/about'}>
                  {t('О сайте')}
              </AppLink>
          </div>
      </div>
  );
};
