import webpack from 'webpack';
import { buildDevServer } from './buildDevServer';
import { buildLoaders } from './buildLoaders';
import { buildPlugins } from './buildPlugins';
import { buildResolvers } from './buildResolvers';

import { BuildOptions } from './types/config';

export function buildWebpackConfig (
  options: BuildOptions,
): webpack.Configuration {
  const { mode, paths, isDev } = options;

  return {
    mode: mode,
    entry: paths.entry, // указываем путь к корневому файлу, используем path т.к. на разные ос по разному работают с путями и могут быть вопросики
    output: {
      // то куда и как будем делать сборку приложения
      filename: '[name].[contenthash].js', // название файла
      path: paths.build,
      clean: true,
    },
    plugins: buildPlugins(options),
    module: {
      rules: buildLoaders(options),
    },
    resolve: buildResolvers(options),
    devtool: isDev ? 'inline-source-map' : undefined, //  что бы по стек трейсу определять где ошибки
    devServer: isDev ? buildDevServer(options) : undefined,
  };
}
